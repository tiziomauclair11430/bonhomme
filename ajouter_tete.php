<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajouter Tete</title>
</head>
<body>
    <form action="ajouter_tete.php" method="post" enctype="multipart/form-data">
        <input type="file" name="burl">
        <input type="submit">
    </form>
</body>
</html>

<?php 

$host = 'localhost';
$db = 'bonhomme';
$user = 'moi';
$pass = 'test';

try{
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }catch(PDOException $e){
        echo $e->getMessage();
    }

function ajouter_tete($img){
    global $pdo;
    try
    {
    $pdo->prepare("INSERT INTO tete(burl) VALUES (?)")->execute([$img]);
    }
    catch(PDOException $e)
    {
    echo $e->getMessage();
    }
}

if(
    $_FILES['burl']
){  
    $img = $_FILES['burl']['name'];

    if (move_uploaded_file($_FILES['burl']['tmp_name'], "img/tete/$img")) {
        print "Téléchargé avec succès!";
        ajouter_tete($img);
    } else {
        print "Échec du téléchargement!";
    }
    
}
?>
