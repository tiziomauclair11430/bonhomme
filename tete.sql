DROP DATABASE IF EXISTS bonhomme;
CREATE DATABASE bonhomme;

USE bonhomme;
GRANT ALL PRIVILEGES ON bonhomme.* TO 'moi'@'localhost';

CREATE TABLE tete(
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    burl Varchar(255)
);
