DROP DATABASE IF EXISTS bonhomme;
CREATE DATABASE bonhomme;

USE bonhomme;
GRANT ALL PRIVILEGES ON bonhomme.* TO 'phpmyadmin'@'localhost';

CREATE TABLE pieds (
    id int UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    burl varchar (255)
)
