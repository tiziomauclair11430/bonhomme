DROP DATABASE IF EXISTS corps;
CREATE DATABASE corps;
USE corps;

GRANT ALL PRIVILEGES ON corps.* TO 'bob'@'localhost';
flush privileges;


CREATE TABLE corps(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    burl VARCHAR(55)
);