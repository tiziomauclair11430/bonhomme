<?php
$host = 'localhost';
$db = 'bonhomme';
$user = 'phpmyadmin';
$pass = 'Admin';


$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

if (isset($_POST['valid'])) {
    $photo = $_FILES['burl']['name'];
    if (move_uploaded_file($_FILES['burl']['tmp_name'], "img/$photo")) {
        print "Téléchargé avec succès!";
    } else {
        print "Échec du téléchargement!";
    }
    addpieds($photo);
}

function addpieds($img)
{

    global $pdo;
    $req = $pdo->prepare("INSERT into pieds(burl) values (?)");
    $req->execute([$img]);
};


?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <form action="ajouter_pieds.php" method="POST" enctype="multipart/form-data">
        <input class="fichier" name="burl" type="file" accept="image/*">
        <input type="submit" name="valid" value="ajouter">
    </form>
</body>

</html>