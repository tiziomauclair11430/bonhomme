<?php

include "debug.php";

$host = 'localhost';
$db   = 'keanu';
$user = 'bob';
$pass = 'toto'; // ne pas faire ça dans la vrai vie !

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

$bobData = array(
    "tete" => $pdo->query('SELECT burl FROM tete;')->fetchAll(),
    "corps" => $pdo->query('SELECT burl FROM corps;')->fetchAll(),
    "jambes" => $pdo->query('SELECT burl FROM jambes;')->fetchAll(),
    "pieds" => $pdo->query('SELECT burl FROM pieds;')->fetchAll(),
);


$randomBob = array(
    "tete" => $bobData['tete'][array_rand($bobData['tete'])]['burl'],
    "corps" => $bobData['corps'][array_rand($bobData['corps'])]['burl'],
    "jambes" => $bobData['jambes'][array_rand($bobData['jambes'])]['burl'],
    "pieds" => $bobData['pieds'][array_rand($bobData['pieds'])]['burl'],
);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bonhomme</title>
</head>
<body>
    <h1>BONHOMME</h1>
    <div id="bonhomme">
        <?php foreach($randomBob as $k => $v): ?>
            <img src="<?= $v ?>" alt="<?= $k ?>">
        <?php endforeach; ?>
    </div>
    <style>
        #bonhomme{
            display: flex;
            flex-direction: column;
            width: 200px;
        }
        #bonhomme img{
            max-height: 200px;
        }
    </style>
</body>
</html>